import React, { Component } from "react";
import Itemprops from "./Itemprops";

export default class DemoProps extends Component {
  state = {
    username: "alice",
  };
  render() {
    return (
      <div>
        <Itemprops name={this.state.username} />
      </div>
    );
  }
}
