import logo from "./logo.svg";
import "./App.css";
import ShoeShop from "./ShoeShop/ShoeShop";
import DemoProps from "./Demoprops/DemoProps";
import "./ShoeShop/detail.css";

function App() {
  return (
    <div className="App">
      <ShoeShop />
    </div>
  );
}

export default App;
