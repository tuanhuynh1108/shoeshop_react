import React, { Component } from "react";

export default class DetailShoeShop extends Component {
  render() {
    let { name, price, description, shortDescription, quantity, image } =
      this.props.detailShoe;
    return (
      <div>
        <div
        // class="modal fade"
        // id="exampleModal"
        // tabindex="-1"
        // aria-labelledby="exampleModalLabel"
        // aria-hidden="true"
        >
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
              Detail Shoe
            </h5>
            <button
              type="button"
              class="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <img
            className="card-img-top mx-auto d-block"
            src={image}
            alt="Card image cap"
            style={{ width: 400 }}
          />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">{price}</p>
          </div>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">{quantity}</li>
            <li className="list-group-item">{description}</li>
            <li className="list-group-item">{shortDescription}</li>
          </ul>
        </div>
      </div>
    );

    // return (
    //   <div>
    //     <button
    //       type="button"
    //       class="btn btn-primary btn-lg"
    //       data-toggle="modal"
    //       data-target="#modelId"
    //     >
    //       day la ca i nut detai;
    //     </button>

    //     <div
    //       class="modal fade"
    //       id="modelId"
    //       tabindex="-1"
    //       role="dialog"
    //       aria-labelledby="modelTitleId"
    //       aria-hidden="true"
    //     >
    //       <div class="modal-dialog" role="document">
    //         <div class="modal-content">
    //           <div class="modal-header">
    //             <h5 class="modal-title">Modal title</h5>
    //             <button
    //               type="button"
    //               class="close"
    //               data-dismiss="modal"
    //               aria-label="Close"
    //             >
    //               <span aria-hidden="true">&times;</span>
    //             </button>
    //           </div>
    //           <div class="modal-body">Body</div>
    //           <div class="modal-footer">
    //             <button
    //               type="button"
    //               class="btn btn-secondary"
    //               data-dismiss="modal"
    //             >
    //               Close
    //             </button>
    //             <button type="button" class="btn btn-primary">
    //               Save
    //             </button>
    //           </div>
    //         </div>
    //       </div>
    //     </div>
    //   </div>
    // );
  }
}
