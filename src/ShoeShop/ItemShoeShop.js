import React, { Component } from "react";
export default class ItemShoeShop extends Component {
  render() {
    let { image, price, description, name } = this.props.dataShoe;
    return (
      <div className="row col-6">
        <div className="card text-left">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <h4 className="card-title">{price}</h4>
            <p className="card-text">{description}</p>
            <button
              className="btn btn-success "
              onClick={() => {
                this.props.handleChangeShoeDetail(this.props.dataShoe);
              }}
              // data-toggle="modal"
              // data-target="#exampleModal"
            >
              View Detail
            </button>
            <button
              onClick={() => {
                this.props.handleAddtoCart(this.props.dataShoe);
              }}
              className="btn btn-warning"
            >
              Add
            </button>
          </div>
        </div>
      </div>
    );
  }
}

// id: 1,
//     name: "Adidas Prophere",
//     alias: "adidas-prophere",
//     price: 350,
//     description:
//       "The adidas Primeknit upper wraps the foot with a supportive fit that enhances movement.\r\n\r\n",
//     shortDescription:
//       "The midsole contains 20% more Boost for an amplified Boost feeling.\r\n\r\n",
//     quantity: 995,
//     image: "http://svcy3.myclass.vn/images/adidas-prophere.png",
