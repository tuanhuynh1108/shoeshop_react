import React, { Component } from "react";
import ItemShoeShop from "./ItemShoeShop";

export default class ListShoeShop extends Component {
  renderListShoe = () => {
    return this.props.list.map((item, index) => {
      return (
        <ItemShoeShop
          dataShoe={item}
          handleChangeShoeDetail={this.props.handleViewShoe}
          key={index}
          handleAddtoCart={this.props.handleAddtoCart}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
