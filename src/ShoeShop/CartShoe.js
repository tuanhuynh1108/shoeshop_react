import React, { Component } from "react";

export default class CartShoe extends Component {
  renderContentCart = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <img src={item.image} style={{ width: 80 }} />
          </td>
          <td>
            <button
              className="btn btn-primary"
              onClick={() => {
                this.props.handleChangeQuantity(item.id, -1);
              }}
            >
              -
            </button>
            <strong className="mx-3">{item.soLuong}</strong>
            <button
              className="btn btn-warning"
              onClick={() => {
                this.props.handleChangeQuantity(item.id, 1);
              }}
            >
              +
            </button>
          </td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.handleDelete(item.id, 1);
              }}
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Price</th>
              <th>Image</th>
              <th>Quantity</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderContentCart()}</tbody>
        </table>
      </div>
    );
  }
}

// id: 1,
// name: "Adidas Prophere",
// alias: "adidas-prophere",
// price: 350,
// description:
//   "The adidas Primeknit upper wraps the foot with a supportive fit that enhances movement.\r\n\r\n",
// shortDescription:
//   "The midsole contains 20% more Boost for an amplified Boost feeling.\r\n\r\n",
// quantity: 995,
// image:
