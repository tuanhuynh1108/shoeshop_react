import React, { Component } from "react";
import CartShoe from "./CartShoe";
import DetailShoeShop from "./DetailShoeShop";
import ListShoeShop from "./ListShoeShop";
import { shoeArr } from "./dataShoe";

export default class ShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    detailShoe: shoeArr[0],
    cart: [],
  };
  handleChangeShoe = (shoe) => {
    this.setState({
      detailShoe: shoe,
    });
  };
  handleAddtoCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      // let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push({ ...shoe, soLuong: 1 });
    } else {
      cloneCart[index].soLuong++;
    }
    this.setState({ cart: cloneCart });
  };
  handleDelete = (id) => {
    let clonecart = [...this.state.cart];

    let index = clonecart.findIndex((shoe) => {
      return shoe.id == id;
    });

    clonecart.splice(index, 1);
    this.setState({ cart: clonecart });
  };
  handleChangeQuantity = (id, luaChon) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((shoe) => {
      return shoe.id == id;
    });
    cloneCart[index].soLuong = cloneCart[index].soLuong + luaChon;
    // cloneCart[index].soLuong == 0 && cloneCart[index].splice(index, 1);
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div className="row">
        <div className="col-6">
          <CartShoe
            cart={this.state.cart}
            handleDelete={this.handleDelete}
            handleChangeQuantity={this.handleChangeQuantity}
          />
        </div>
        <div className="col-6">
          <ListShoeShop
            handleViewShoe={this.handleChangeShoe}
            list={this.state.shoeArr}
            handleAddtoCart={this.handleAddtoCart}
          />
        </div>
        <DetailShoeShop detailShoe={this.state.detailShoe} />
      </div>
    );
  }
}
